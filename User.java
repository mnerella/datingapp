public class User {

public String name;
public int age;
public String profile;
public String primary_Hobby;
public String[] hobbies;
public float height;
public int weight;
public int salary;

public String food_Preference;
public String primary_Food;
public String[] interest_Food_Items;
public String location;

Gender gender_type;
MaritalStatus marital_Status;







/**
* create a method i.e createNewBlunderUser
*
* @param profileName
* @param age
* @param designation
* @param hobby
* @return
*/
public User(String name, int age, Gender gender_type,float height,int weight,String profile,int salary,MaritalStatus marital_Status,String food_Preference,String location,String primary_Hobby,String[] hobbies,String fav_Food,String[] food_Items) {
this.name = name;
this.age = age;
this.profile= profile;
this.gender_type=gender_type;
this.height=height;
this.weight=weight;
this.salary=salary;
this.marital_Status=marital_Status;
            this.food_Preference=food_Preference;
            this.location=location;
            this.primary_Hobby= primary_Hobby;    
            this.hobbies=hobbies;
            this.primary_Food=fav_Food;
            this.interest_Food_Items=food_Items;


System.out.println(this);



}

/**
* Overriding the default toString method of User so that User can be printed in
* human readable format.
*/
public String toString() {

String stringToReturn = "";

stringToReturn += "               Display the All user details             \n";
stringToReturn += "==================================================" + "\n";
stringToReturn += "name                        :                " + this.name + "\n";
stringToReturn += "Age                         :                " + this.age + "\n";
stringToReturn += "Gender                      :                " + this.gender_type + "\n";
stringToReturn += "Height                      :                " + this.height + "\n";
stringToReturn += "Weight                      :                " + this.weight + "\n";
stringToReturn += "Salary                      :                " + this.salary + "\n";
stringToReturn += "Marrital_Status             :                " + this.marital_Status + "\n";
stringToReturn += "Food_Preference             :                " + this.food_Preference + "\n";
stringToReturn += "Location                    :                " + this.location + "\n";
stringToReturn += "Primary_Hobby               :                " + this.primary_Hobby + "\n";
stringToReturn += "Primary_Food                :                " + this.primary_Food + "\n";






String hobbyString = "[";
for (int i = 0; i < this.hobbies.length; i++) {
hobbyString += "\"" + this.hobbies[i] + "\",";
}
hobbyString += "]";

stringToReturn += "Hobbies are                 :                " + hobbyString + "\n";




String foodString ="[";
for(int i=0;i<this.interest_Food_Items.length;i++) {
foodString+="\""+this.interest_Food_Items[i] + "\",";
   }
foodString +="]";

stringToReturn +="Food items are              :               "  + foodString +"\n";
stringToReturn +="***********************************************************************"+"\n";
System.out.println("\n");
            return stringToReturn;  
}


}



