

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
/**
 * This is the implementation of main class
 * @author mnerella
 *
 */
public class Main {
public static void main(String[] args) {

Scanner scn=new Scanner(System.in);
//Creating a object
UserCollection u=new UserCollection();
//Gender as a enum
Gender g1  = Gender.Male;
Gender g2 = Gender.Female;
Gender g3 = Gender.PreferNotToSay;

//MaritalStatus as a enum type
MaritalStatus m1 =MaritalStatus.Married;
MaritalStatus m2 =MaritalStatus.UnMarried;


u.createUser("Sharath" , 23,g1,5.3F,75, "Software Trainee Engineer" ,60000,m2, "Non-veg","Guntur","Cricket"  ,new String[] {"Playing","Cricket","Watching"},"Chicken",new String[] {"rice","chapathi","chicken"});
u.createUser("Chandra" ,25,g1,5.8F,59, "Senior Software Engineer" ,80000,m1, "Non-veg","Hyderabad","Coding"  ,new String[] {"Watching","Coding","Talking"},"Chapathi",new String[] {"Bhajji","mutton","dosa"});
u.createUser("Sumith" ,18,g3,5.4F,64, "Senior data Engineer" ,70000,m2, "Non-veg","Guntur","Reading"  ,new String[] {"Reading","Coding","Teaching"},"Dosa",new String[] {"Rice","Fish","Vada"});
u.createUser("Sairaju" ,19,g1,6.0F,71, "Senior Web developer" ,25000,m1, "Non-veg","Vijayawada","Playing"  ,new String[] {"Watching","Playing","Talking"},"Biryani",new String[] {"Bhajji","Parotta","dosa"});
u.createUser("Maneesha" ,23,g2,6.2F,63, "Software Trainee Engineer" ,30000,m2, "veg","Vizag","Watching"  ,new String[] {"Watching","Reading","Teaching"},"Puri",new String[] {"LegPiece","Biryani","Bonda"});
u.createUser("Jeevan" ,21,g1,5.0F,64, "Java developer" ,40000,m2, "Non-veg","Tuni","Planning"  ,new String[] {"Drawing","Coding","Playing"},"Chicken",new String[] {"Panipuri","Eggbajji","parotta"});
u.createUser("Naveen" ,25,g1,5.7F,64, "Data analyst" ,50000,m2, "Non-veg","Adigoppula","Coding"  ,new String[] {"Laughing","Playing","Training"},"Bhajji",new String[] {"Manchuria","ChickenPakodi","Noodles"});
u.createUser("Mani" ,27,g1,5.8F,64, "Java developer" ,60000,m1, "Non-veg","Warangal","Playing"  ,new String[] {"Eating","Talking","Walking"},"Biryani",new String[] {"LegPiece","Biryani","Bonda"});
u.createUser("Jahnavi" ,16,g2,5.8F,54, "Senior Software Tester" ,70000,m1, "Non-veg","Hyderabad","Coding"  ,new String[] {"Playing","Running","Drinking"},"Biryani",new String[] {"rice","chapathi","chicken"});
u.createUser("Manogna" ,25,g2,5.4F,64, "Senior Software Engineer" ,80000,m1, "Non-veg","Bihar","Cricket"  ,new String[] {"Watching","Jogging","Teaching"},"Friedrice",new String[] {"Bhajji","mutton","dosa"});













System.out.println("=======================This is my project details========================");
    System.out.println("1. Name ");
    System.out.println("2. Location. ");
    System.out.println("3. Profile");
    System.out.println("4. Primary_Hobbie");
    System.out.println("5. HobbieOR");
    System.out.println("6. HobbieAND");
    System.out.println("7. Food_Preference");
    System.out.println("8. Fav_Food");
    System.out.println("9. FoodOR");
    System.out.println("10.FoodAND");
    System.out.println("11.Exact_Age");
    System.out.println("12.Min_Age");
    System.out.println("13.Max_Age");
    System.out.println("14.Between_Age");
    System.out.println("15.Gender");
    System.out.println("16.Exact Height");
    System.out.println("17.Min_Height");
    System.out.println("18.Max_Height");
    System.out.println("19.Between_Height");
    System.out.println("20.Exact Weight");
    System.out.println("21.Min_Weight");
    System.out.println("22.Max_Weight");
    System.out.println("23.Between_Weight");
    System.out.println("24.Exact_Salary");
    System.out.println("25.Exact_Min_Salary");
    System.out.println("26.Exact_Max_Salary");
    System.out.println("27.Exact_Range_Salary");
    System.out.println("28.Marital_Status");
    //Select a choice
    System.out.print("Enter your Choice:");
    int choice=scn.nextInt();

   if(choice==1) {
//create a method for  searching the names
ArrayList<User> res1 = u.getMatchName("Ashok");
System.out.println(res1);
System.out.println(" Total Names matched:"+res1.size());
   }
   else if(choice==2) {
//create a method for searching the location
ArrayList<User> res2 = u.getMatchLocation("Guntur");
System.out.println(res2);
System.out.println("Total Locations Matched:"+res2.size());
}
   else if(choice==3) {
//create a method for searching the profile
ArrayList<User> res3 = u.getMatchProfile("Java developer");
System.out.println(res3);
System.out.println("Total Profiles Matched:"+res3.size());
   }
   else if(choice==4) {
//create a method for searching the primary hobby

ArrayList<User> res4 = u.getMatchPrimary_Hobby("Coding");
System.out.println(res4);
System.out.println("Total Primary_Hobbies Matched:"+res4.size());
   }
   else if(choice==5) {
//create a method for searching the OR hobbies

HashSet<User> res5 = u.getMatchHobbies(new String[]{"Coding","Laughing"});
System.out.println(res5);
System.out.println("Total OR hobbies Matched:"+res5.size());
   }
   else if(choice==6) {
//create a method for searching the AND hobbies
ArrayList<User> res6 = u.getHobbyAndList(new String[] {"Laughing","Playing"});
System.out.println(res6);
System.out.println("Hobbies And List:"+res6.size());
   }
   else if(choice==7) {
//create a method for searching the food_Preference

ArrayList<User> res7=u.getMatch_Food_Preference("veg");
System.out.println(res7);
System.out.println("Food preference list:"+res7.size());
   }
   else if(choice==8) {
//create a method for searching the favorite food

ArrayList<User> res8=u.getMatch_Favourite_Food("Biryani");
System.out.println(res8);
System.out.println("Food preference list:"+res8.size());
   }
   else if(choice==9) {
//create a method for searching the food OR list

ArrayList<User> res9=u.getMatchFoodOR(new String[]{"Biryani","Fish",});
System.out.println(res9);
System.out.println("Food preference list:"+res9.size());
   }
   else if(choice==10) {
//create a method for searching the food AND list

ArrayList<User> res10=u.getMatchFoodAND(new String[]{"Manchuria","Noodles"});
System.out.println(res10);
System.out.println("Food preference list:"+res10.size());
   }
   else if (choice==11) {
   //create a method for searching the Exact age
   ArrayList<User> res11=u.getMatchExactage(25);
System.out.println(res11);
System.out.println("Food preference list:"+res11.size());
   
   }
   else if(choice==12) {
   //Create a method for searching the minimum age
    ArrayList<User> res12=u.getMatchMinage(20);
System.out.println(res12);
System.out.println("Food preference list:"+res12.size());
   
   }
   else if(choice==13) {
   //Create a method for searching the maximum age
ArrayList<User> res13=u.getMatchMaxage(24);
System.out.println(res13);
System.out.println("Food preference list:"+res13.size());
   
 }
   else if(choice==14) {
//Create a method for searching the maximum age
ArrayList<User> res14=u.getMatchBetweenage(18,22);
    System.out.println(res14);
    System.out.println("Food preference list:"+res14.size());
   
}
   else if(choice==15) {
   //create a method for searching the gender
   ArrayList<User> res15=u.getMatchgender(g3);
   System.out.println(res15);
   System.out.println("Gender list:"+res15.size());
   }
   else if(choice==16) {
   //create a method for searching the Exact height user
    ArrayList<User> res16=u.getMathExactHeight(5.5F);
    System.out.println(res16);
    System.out.println("Exact Height list:"+res16.size());
   }
   else if(choice==17) {
 //create a method for searching the Min height user
  ArrayList<User> res17=u.getMatchMinHeight(5.5F);
  System.out.println(res17);
  System.out.println("Exact Min Height list:"+res17.size());
}
   else if(choice==18) {
 //create a method for searching the Max height user
 ArrayList<User> res18=u.getMatchMaxHeight(5.4F);
 System.out.println(res18);
 System.out.println("Exact  MaxHeight list:"+res18.size());
}
   else if(choice==19) {
    //create a method for searching the between height user
 ArrayList<User> res19=u.getMatchBetweenHeight(5.4F,6.0F);
 System.out.println(res19);
 System.out.println("Exact Height list:"+res19.size());
   
   }
   else if(choice==20) {
   //create a method for searching the Exact weight user
    ArrayList<User> res20=u.getMathExactWeight(64);
    System.out.println(res20);
    System.out.println("Exact Weight list:"+res20.size());
   }
   else if(choice==21) {
 //create a method for searching the Min weight user
  ArrayList<User> res21=u.getMatchMinWeight(55);
  System.out.println(res21);
  System.out.println("Exact Min Weight list:"+res21.size());
}
   else if(choice==22) {
 //create a method for searching the Max weight user
 ArrayList<User> res22=u.getMatchMaxWeight(54);
 System.out.println(res22);
 System.out.println("Exact  MaxWeight list:"+res22.size());
}
   else if(choice==23) {
    //create a method for searching the between weight user
 ArrayList<User> res23=u.getMatchBetweenWeight(50,60);
 System.out.println(res23);
 System.out.println("Exact between weight list:"+res23.size());
   
   }
   else if(choice==24) {
    //create a method for searching the exact salary
    ArrayList<User> res24= u.getMatchExactSalary(60000);
    System.out.println(res24);
    System.out.println("Exact salary list:"+res24.size());
   }
   else if(choice==25) {
    //create a method for searching the min salary
    ArrayList<User> res25 =u.getMatchMinsalary(70000);
    System.out.println(res25);
    System.out.println("Exact Min salary:"+res25.size());
   }
   else if(choice==26) {
    //create a method for searching the max salary
    ArrayList<User> res26 =u.getMatchMaxsalary(50000);
    System.out.println(res26);
    System.out.println("Exact Min salary:"+res26.size());
   }
   else if(choice==27) {
    //create a method for searching salary in between range
    ArrayList<User> res27 =u.getMatchRangesalary(25000,50000);
    System.out.println(res27);
    System.out.println("Exact Min salary:"+res27.size());
   }
   else if(choice==28) {
    //create a method for searching the Married status
    ArrayList<User> res28 =u.getMatchMariedStatus(m2);
    System.out.println(res28);
    System.out.println("Exact Min salary:"+res28.size());
   
   }


}
}


