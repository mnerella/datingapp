import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;



/**
 * This is the implementation of AllUsers class
 *
 * @author mnerella
 *
 */

public class UserCollection {

ArrayList<User> listOfAllUsers = new ArrayList<User>();


/**
* This is a method for creating a new user
*
* @param name
* @param age
* @param gender
* @param height
* @param weight
* @param profile
* @param salary
* @param marital_Status
* @param food_Preference
* @param location
* @param primary_Hobby
* @param hobbies
* @param fav_Food
* @param food_Items
* @return
*/
public boolean createUser(String name, int age, Gender gender_type, float height, int weight, String profile, int salary,
MaritalStatus marital_Status, String food_Preference, String location, String primary_Hobby, String[] hobbies,
String fav_Food, String[] food_Items) {
User newUser = new User(name, age, gender_type, height, weight, profile, salary, marital_Status, food_Preference,
location, primary_Hobby, hobbies, fav_Food, food_Items);
this.listOfAllUsers.add(newUser);
return true;
}



/**
* This is a method to perform the name matching
*
* @param string
* @return
*/
public ArrayList<User> getMatchName(String string) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("                 Matched Names list               ");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {

if (currUser.name == string)
set.add(currUser);

}

return set;
}

/**
* This is a method to perform the location matching
*
* @param loc
* @return
*/
public ArrayList<User> getMatchLocation(String loc) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("                 Matched Locations list               ");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {

if (currUser.location == loc)
set.add(currUser);

}

return set;
}

/**
* This is a method to perform the profile matching
*
* @param profile
* @return
*/
public ArrayList<User> getMatchProfile(String profile) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("                 Matched Profiles list               ");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {

if (currUser.profile == profile)
set.add(currUser);

}

return set;
}

/**
* This is a method to perform the Primary_hobby matching
*
* @param hobby
* @return
*/
public ArrayList<User> getMatchPrimary_Hobby(String hobby) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("                 Matched Primary_Hobby list               ");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {

if (currUser.primary_Hobby == hobby)
set.add(currUser);

}
return set;

}

/**
* This is a method to perform the hobby OR matching
*
* @param playthings
* @return
*/
public HashSet<User> getMatchHobbies(String[] hobbyOR) {
HashSet<User> getHobbyORlist = new HashSet<User>();
System.out.println("                 Matched Hobbies OR list               ");
System.out.println("======================================================");
for (int i = 0; i < this.listOfAllUsers.size(); i++) {
User newuserfound = this.listOfAllUsers.get(i);
for (int j = 0; j < newuserfound.hobbies.length; j++) {
for (int k = 0; k < hobbyOR.length; k++) {
if (newuserfound.hobbies[j] == hobbyOR[k]) {
getHobbyORlist.add(newuserfound);
}
}
}
}
return getHobbyORlist;
}

/**
* This is a method to perform the hobby AND matching
*
* @param hobbyAnd
* @return
*/
public ArrayList<User> getHobbyAndList(String[] hobbyAnd) {
ArrayList<User> getHobbyAndlist = new ArrayList<User>();
System.out.println("                 Matched Hobbies AND list               ");
System.out.println("======================================================");
User newuser = null;
String hobby1 = hobbyAnd[0];
String hobby2 = hobbyAnd[1];
for (User currentuser : this.listOfAllUsers) {
if ((Arrays.asList(currentuser.hobbies).contains(hobby1)
&& (Arrays.asList(currentuser.hobbies).contains(hobby2)))) {
newuser = currentuser;
getHobbyAndlist.add(newuser);
}
}

return getHobbyAndlist;
}

/**
* This is a method to perform the food_Preference either veg or nonveg
*
* @param preference
* @return
*/
public ArrayList<User> getMatch_Food_Preference(String preference) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("                 Matched Food_Preference list               ");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {

if (currUser.food_Preference == preference)
set.add(currUser);

}

return set;

}

/**
* This is a method to perform the favourite food
*
* @param favourite_Food
* @return
*/

public ArrayList<User> getMatch_Favourite_Food(String favourite_Food) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("                 Matched Favourite_Food list               ");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {

if (currUser.primary_Food == favourite_Food)
set.add(currUser);

}
return set;

}

/**
* This is a method to perform the hobby OR matching
*
* @param foodOR
* @return
*/
public ArrayList<User> getMatchFoodOR(String[] foodOR) {
ArrayList<User> getFoodORlist = new ArrayList<User>();
System.out.println("                 Matched Food OR list               ");
System.out.println("======================================================");
for (int i = 0; i < this.listOfAllUsers.size(); i++) {
User newuserfound = this.listOfAllUsers.get(i);
for (int j = 0; j < newuserfound.interest_Food_Items.length; j++) {
for (int k = 0; k < foodOR.length; k++) {
if (newuserfound.interest_Food_Items[j] == foodOR[k]) {
getFoodORlist.add(newuserfound);
}
}
}
}
return getFoodORlist;
}

/**
* This is a method to perform the hobby AND matching
*
* @param foodAND
* @return
*/

public ArrayList<User> getMatchFoodAND(String[] foodAND) {
ArrayList<User> getFoodAndlist = new ArrayList<User>();
System.out.println("                 Matched Food AND list               ");
System.out.println("======================================================");
User newuser = null;
String food1 = foodAND[0];
String food2 = foodAND[1];
for (User currentuser : this.listOfAllUsers) {
if ((Arrays.asList(currentuser.interest_Food_Items).contains(food1)
&& (Arrays.asList(currentuser.interest_Food_Items).contains(food2)))) {
newuser = currentuser;
getFoodAndlist.add(newuser);
}
}

return getFoodAndlist;
}

/**
* This is a method to perform the age Exactly find
*
* @param age
* @return
*/

public ArrayList<User> getMatchExactage(int age) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("                 Matched Age list               ");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {

if (currUser.age == age)
set.add(currUser);

}

return set;

}

/**
* This is a method to perform the Minimum age find
*
* @param min
* @return
*/

public ArrayList<User> getMatchMinage(int min) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("                 Matched Min Age list               ");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {

if (currUser.age >= min)
set.add(currUser);

}

return set;

}

/**
* This is a method to perform the Maximum age find
*
* @param max
* @return
*/
public ArrayList<User> getMatchMaxage(int max) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("                 Matched Max Age list               ");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {

if (currUser.age <= max)
set.add(currUser);

}

return set;

}

/**
* This is a method to find the beween the range of age
*
* @param max
* @return
*/
public ArrayList<User> getMatchBetweenage(int min, int max) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("                 Matched Max Age list               ");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {

if (currUser.age <= max && currUser.age >= min)
set.add(currUser);

}

return set;

}

/**
* This is a method to perform the searching the gender
*
* @param gen
* @return
*/
public ArrayList<User> getMatchgender(Gender gen) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("       Matched Searching Gender         ");
System.out.println("=========================================");
for (User currUser : listOfAllUsers) {
if (currUser.gender_type == gen) {
set.add(currUser);
}

}
return set;
}

/**
* This is a method to perform the searching the exact height
*
* @param d
* @return
*/
public ArrayList<User> getMathExactHeight(float d) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("        Matched Searching Exact height    ");
System.out.println("=============================================");
for (User currUser : listOfAllUsers) {
if (currUser.height == d) {
set.add(currUser);
}

}
return set;
}

/**
* This is method to perform the searching the min height
*
*/
public ArrayList<User> getMatchMinHeight(float min) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("     Matched searching the min height  ");
System.out.println("=========================================");
for (User currUser : listOfAllUsers) {
if (currUser.height >= min) {
set.add(currUser);
}

}
return set;
}

/**
* This is a method to perform the Height maximum
*
* @param max
* @return
*/
public ArrayList<User> getMatchMaxHeight(float max) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("     Matched searching the max height  ");
System.out.println("=========================================");
for (User currUser : listOfAllUsers) {
if (currUser.height <= max) {
set.add(currUser);
}

}
return set;
}

/**
* This is a method to perform the range of height
*
* @param min
* @param max
* @return
*/

public ArrayList<User> getMatchBetweenHeight(float min, float max) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("                 Matched Between height list               ");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {

if (currUser.height <= max && currUser.height >= min)
set.add(currUser);

}

return set;

}

/**
* This is a method to perform the Exact weight
*
* @param wgt
* @return
*/
public ArrayList<User> getMathExactWeight(int wgt) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("        Matched Searching Exact weight    ");
System.out.println("=============================================");
for (User currUser : listOfAllUsers) {
if (currUser.weight == wgt) {
set.add(currUser);
}

}
return set;
}

/**
* This is method to perform the searching the min weight
*
* @param min
* @return
*/
public ArrayList<User> getMatchMinWeight(int min) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("     Matched searching the min weight  ");
System.out.println("=========================================");
for (User currUser : listOfAllUsers) {

if (currUser.weight >= min) {

}

}
return set;
}

/**
* This is a method to perform the Searching the maximum weight
*
* @param max
* @return
*/
public ArrayList<User> getMatchMaxWeight(int max) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("     Matched searching the max weight  ");
System.out.println("=========================================");
for (User currUser : listOfAllUsers) {
if (currUser.weight <= max) {
set.add(currUser);
}

}
return set;
}

/**
* This is a method to perform the searching range of weight
*
* @param min
* @param max
* @return
*/
public ArrayList<User> getMatchBetweenWeight(int min, int max) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("                 Matched Between weight list               ");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {

if (currUser.weight <= max && currUser.weight >= min) {
set.add(currUser);

}
}

return set;

}

/**
* This is a method to perform the searching the exact salary
*
* @param sal
* @return
*/
public ArrayList<User> getMatchExactSalary(int sal) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("                Matched the Exact salary              ");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {
if (currUser.salary == sal) {
set.add(currUser);
}
}
return set;
}

/**
* This is a method to perform the salary minimum
*
* @param minsal
* @return
*/
public ArrayList<User> getMatchMinsalary(int minsal) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("               Matched the Min salary");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {
if (currUser.salary >= minsal) {
set.add(currUser);
}
}
return set;
}

/**
* This is a method to perform the salary maximum
*
* @param maxsal
* @return
*/
public ArrayList<User> getMatchMaxsalary(int maxsal) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("               Matched the Max salary");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {
if (currUser.salary <= maxsal) {
set.add(currUser);
}
}
return set;
}

/**
* This is a method to perform the range in between a salary
*
* @param min
* @param max
* @return
*/
public ArrayList<User> getMatchRangesalary(int min, int max) {
ArrayList<User> set = new ArrayList<User>();
System.out.println("                 Matched Between salary list               ");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {

if (currUser.salary <= max && currUser.salary >= min) {
set.add(currUser);

}
}

return set;

}
    /**
     * Create a method to  perform the matching the course_names
     * @param course
     * @return
     */


   /**
    * This is a method to perform the maried status
    * @param m2
    * @return
    */
public ArrayList<User> getMatchMariedStatus(MaritalStatus m2) {

ArrayList<User> set = new ArrayList<User>();
System.out.println("                 Matched Married Status list               ");
System.out.println("======================================================");
for (User currUser : listOfAllUsers) {
  if(currUser.marital_Status==m2) {
  set.add(currUser);
  }
 
    }
return set;
}
}

